import json
import random
from inflection import underscore, parameterize
from app.db.session import db_session

import app.queries.voter as voter_query
from app.models.voter import VoterIn

# Load bobburgers.json
file = open('app/db/bobburgers.json')
content = file.read()
data = json.loads(content)

names = [name for entry in data for name in [
    entry['actor'], *entry['characters']]]

for name in names:
    if not voter_query.get_by_name(db_session, name=name):
        # Create 10 digit random phone number
        phone_number = ''.join(
            map(str, [1, *[random.randint(0, 9) for x in range(1, 10)]]))
        underscore_name = underscore(parameterize(name))
        email = f"{underscore_name}@bobburgers.com"

        voter = VoterIn(name=name, email=email, phone_number=phone_number)
        voter_query.create(db_session, voter_in=voter)
