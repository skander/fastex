from sqlalchemy import Integer, Column, DateTime, func
from sqlalchemy.ext.declarative import declarative_base, declared_attr

from inflection import underscore, pluralize

class CustomBase(object):
    # Generate __tablename__ automatically
    @declared_attr
    def __tablename__(cls):
        return pluralize(underscore(cls.__name__))

    id = Column(Integer, primary_key=True, index=True, unique=True, autoincrement=True)
    created_at = Column(DateTime, server_default=func.now())
    updated_at = Column(DateTime, server_default=func.now(), server_onupdate=func.now())


if __debug__:
    # monkey-patch in useful repr() for all objects, but only in dev
    def tablerepr(self):
        return "<{}({})>".format(
            self.__class__.__name__,
            ', '.join(
                ["{}={}".format(k, repr(self.__dict__[k]))
                    for k in sorted(self.__dict__.keys())
                    if k[0] != '_']
            )
        )

    CustomBase.__repr__ = tablerepr


Base = declarative_base(cls=CustomBase)
