from typing import Optional, List
from sqlalchemy.orm import Session
from sqlalchemy import func

from ..db.base import Voter
from ..db.base import VoterAlert
from ..models.voter import VoterIn

def get(db_session: Session, *, voter_id: int) -> Optional[Voter]:
    return db_session.query(Voter).filter(Voter.id == voter_id).first()

def get_by_email(db_session: Session, *, email: str) -> Optional[Voter]:
    return db_session.query(Voter).filter(Voter.email == email).first()

def get_by_name(db_session: Session, *, name: str) -> Optional[Voter]:
    return db_session.query(Voter).filter(Voter.name == name).first()

def create(db_session: Session, *, voter_in: VoterIn) -> Voter:
    voter = Voter(
        email=voter_in.email,
        name=voter_in.name,
        phone_number=voter_in.phone_number,
    )

    if existing_voter := get_by_name(db_session, name=voter.name):
        return existing_voter

    db_session.add(voter)
    db_session.commit()
    db_session.refresh(voter)
    return voter

def voter_ids_present(db_session: Session, *, ids: List[int]) -> List[int]:
    return [id[0] for id in db_session.query(Voter.id).filter(Voter.id.in_(ids)).all()]

def all_voter_ids(db_session: Session) -> List[int]:
    return [id[0] for id in db_session.query(Voter.id).all()]

def unread_voter_alerts(db_session: Session, *, voter: Voter) -> List[VoterAlert]:
    return db_session.query(VoterAlert).filter(VoterAlert.voter_id == voter.id, VoterAlert.read == False).all()
