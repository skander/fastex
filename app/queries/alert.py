from typing import Optional
from sqlalchemy.orm import Session

from ..db.base import Alert as AlertOrm

from ..models.alert import AlertIn, Alert
from ..models.voter import VoterIn
from ..models.voter_alert import VoterAlert, VoterAlertIn

from .voter import voter_ids_present, all_voter_ids
from .voter_alert import voter_alert_create

def alert_create(db_session: Session, *, alert_in: AlertIn) -> Optional[Alert]:
    voter_ids = []
    missing_voter_ids = []
    errors = []

    # Any voter ids added
    if alert_in.voter_ids:
        voter_ids = voter_ids_present(db_session, ids=alert_in.voter_ids)

        # No voter IDs present
        if len(voter_ids) == 0:
            errors = [f"No voter found for ID {id}" for id in alert_in.voter_ids]
        elif missing_voter_ids := list(set(alert_in.voter_ids) - set(voter_ids)): # Incorrect use?
            errors = [f"No voter found for ID {id}" for id in missing_voter_ids]
    else:
        voter_ids = all_voter_ids(db_session)

    if voter_ids:
        alert_db = AlertOrm(message=alert_in.message)
        db_session.add(alert_db)
        db_session.commit()

        for voter_id in voter_ids:
            voter_alert_in=VoterAlertIn(alert_id=alert_db.id, voter_id=voter_id)
            voter_alert_create(db_session, voter_alert_in=voter_alert_in)

        db_session.refresh(alert_db)

        alert = Alert.from_orm(alert_db)
        alert.voter_alert_count = len(voter_ids)
    else:
        alert = Alert(message=alert_in.message)

    alert.errors = errors
    return alert


def dispatch_voter_alerts(alert: Alert):
    alert.voter_alerts
    pass

async def dispatch_voter_alert():
    pass
