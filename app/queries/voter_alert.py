from sqlalchemy.orm import Session
from ..db.base import VoterAlert
from ..models.voter_alert import VoterAlertIn

def voter_alert_create(db_session: Session, *, voter_alert_in: VoterAlertIn) -> VoterAlert:
    voter_alert = VoterAlert(voter_id=voter_alert_in.voter_id, alert_id=voter_alert_in.alert_id)

    db_session.add(voter_alert)
    db_session.commit()
    db_session.refresh(voter_alert)

    return voter_alert

def voter_alert_mark_read(db_session: Session, *, voter_alert: VoterAlert):
    db_session.query(VoterAlert).filter(VoterAlert.id == voter_alert.id).update({'read': True})
