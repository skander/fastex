from fastapi import FastAPI, Header, HTTPException
from pydantic import BaseModel, EmailStr
from typing import List

from starlette.websockets import WebSocket
from starlette.responses import FileResponse

from app.models.voter import Voter, VoterIn
from app.queries.voter import create as voter_create
from app.queries.voter import unread_voter_alerts as unread_alerts
from app.queries.voter import get as voter_get

from app.models.alert import Alert, AlertIn
from app.queries.alert import alert_create

from .db.session import db_session

app = FastAPI()

@app.get("/")
def index():
    return FileResponse('app/views/index.html')

@app.post("/voter", response_model = Voter)
def create_voter(*, voter_in: VoterIn):
    voter = voter_create(db_session, voter_in=voter_in)
    if not voter:
        not_found(errors=["Voter could not be created."])
    return voter

@app.post("/alert", response_model = Alert)
async def alert_voter(*, alert_in: AlertIn):
    alert = alert_create(db_session, alert_in=alert_in)
    # Show 404 for complete failure, 200 for partial error
    # TODO: dispatch ws voter_alerts
    # https://fastapi.tiangolo.com/tutorial/background-tasks/
    if alert.errors and alert.voter_alert_count == 0:
        return not_found(errors=alert.errors)
    else:
        return alert

# https://fastapi.tiangolo.com/tutorial/websockets/
import asyncio
@app.websocket("/inbox/{id}")
async def inbox(websocket: WebSocket, id: int):
    await websocket.accept()
    voter = voter_get(db_session, voter_id: id)
    voter_alerts = unread_alerts(db_session, voter: voter)
    for voter_alert in voter_alerts:
        # send websocket voter alert
        pass

    # While true, send alerts as they come in
    while True:
        # TODO: await subscribe on voter_alerts with voter_id == id
        await websocket.send_json({"hello": id})
        await asyncio.sleep(5)


# https://fastapi.tiangolo.com/tutorial/additional-responses/
def not_found(errors: List[str]):
    raise HTTPException(status_code=404, detail=errors)
