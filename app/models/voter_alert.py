from pydantic import BaseModel

# Shared properties
class VoterAlertBase(BaseModel):
    voter_id: int
    alert_id: int
    read: bool = False

# Stored in DB
class VoterAlertInDBBase(VoterAlertBase):
    id: int = None

class VoterAlertIn(VoterAlertInDBBase):
    pass

# VoterAlert create
class VoterAlertCreate(VoterAlertInDBBase):
    pass

# VoterAlert update
class VoterAlertUpdate(VoterAlertInDBBase):
    pass

# API response
class VoterAlert(VoterAlertInDBBase):
    class Config:
        orm_mode = True
