from typing import List, Optional
from pydantic import BaseModel

from .voter_alert import VoterAlert

# Shared properties
class AlertBase(BaseModel):
    message: str

# Alert create
class AlertIn(AlertBase):
    voter_ids: List[int] = []

# API response
class Alert(AlertBase):
    id: int = None
    voter_alerts: List[VoterAlert] = []
    errors: List[str] = None
    voter_alert_count: int = 0

    class Config:
        orm_mode = True
