from typing import List, Optional
from pydantic import BaseModel, EmailStr

from .voter_alert import VoterAlert

# Shared properties
class VoterBase(BaseModel):
    name: Optional[str] = None
    email: EmailStr = None
    phone_number: str = None

# Stored in DB
class VoterInDBBase(VoterBase):
    id: int = None

class VoterIn(VoterBase):
    pass

# Voter create
class VoterCreate(VoterInDBBase):
    pass

# API response
class Voter(VoterInDBBase):
    voter_alerts: List[VoterAlert] = []

    class Config:
        orm_mode = True
