from sqlalchemy import Column, ForeignKey, Integer, String
from sqlalchemy.orm import relationship

from ..db.base_class import Base

class Alert(Base):
    message = Column(String)
    voter_alerts = relationship("VoterAlert", back_populates='alert')
