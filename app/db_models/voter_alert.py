from sqlalchemy import Column, ForeignKey, Integer, Boolean
from sqlalchemy.orm import relationship

from ..db.base_class import Base

class VoterAlert(Base):
    read = Column(Boolean, default=False, nullable=False)
    voter_id = Column(Integer, ForeignKey('voters.id'), nullable=False)
    alert_id = Column(Integer, ForeignKey('alerts.id'), nullable=False)
    voter = relationship("Voter", back_populates='voter_alerts')
    alert = relationship("Alert", back_populates='voter_alerts')
