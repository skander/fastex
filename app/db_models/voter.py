from sqlalchemy import Column, Integer, String, func
from sqlalchemy.ext.hybrid import Comparator, hybrid_property
from sqlalchemy.orm import relationship

from ..db.base_class import Base

class Voter(Base):
    name = Column(String)
    email = Column(String)
    phone_number = Column(String)
    voter_alerts = relationship("VoterAlert", back_populates='voter')

    @hybrid_property
    def email_insensitive(self):
        return self.email.lower()

    @email_insensitive.comparator
    def email_insensitive(cls):
        return CaseInsensitiveComparator(cls.email)

class CaseInsensitiveComparator(Comparator):
    def __eq__(self, other):
        return func.lower(self.__clause_element__()) == func.lower(other)
